from invoke import task, Collection
from cookiecutter.main import cookiecutter
import os


repository = 'gl:franciscocarbonell/odoo_skeleton'
license = 'license'
header_v8 = 'header v8'
header_v15 = 'header v15'
default_addon_out_dir = 'D:\\'


def get_header(version):
    if version in [8, 11]:
        return header_v8
    return header_v15

def get_license(version):
    if version == 15:
        return license
    return ''

def get_manifest_name(version):
    if version == 8:
        return 'openerp'
    if version in [11, 15]:
        return 'manifest'

def available_version():
    return [8, 11, 15]

def clean_module(version, name, output_dir):
    if version is not 15:
        path = os.path.join(output_dir, name, 'LICENSE')

        if os.path.exists(path):
            os.remove(path)

def get_extra_context_keys(name, version):
    return {
        'module_name': name,
        'version': version,
        'header': get_header(version),
        'license': get_license(version),
        'manifest_name': get_manifest_name(version)
    }

@task
def create_addon(c, name, version=8, out_dir=default_addon_out_dir):
    if version not in available_version():
        print('Version is not available')
    else:
        context = get_extra_context_keys(name, version)
        cookiecutter(repository, extra_context=context, output_dir=out_dir, no_input=True)
        print('Module %s created' % name)
        clean_module(version, name, out_dir)
        print('Clean files done')

ns_addons = Collection('addons')
ns_addons.add_task(create_addon)
