from invoke import Collection
from addons import ns_addons

ns = Collection()
ns.add_collection(ns_addons)